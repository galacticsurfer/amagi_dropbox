from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'amagi.views.home', name='home'),
    # url(r'^amagi/', include('amagi.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Django registration urls
    (r'^accounts/', include('registration.backends.default.urls')),

    # UserFile ursl
    url(r'^accounts/profile/', include('dropboxapp.urls')),

    # Dropbox registration
    url(r'^register_dropbox', 'dropboxapp.views.register', name='register'),

    # Sync Dropbox File
    url(r'^sync_dropbox', 'dropboxapp.views.sync', name='sync'),

    (r'^about/$', TemplateView.as_view(template_name="about.html")),

    (r'^contact/$', TemplateView.as_view(template_name="contact.html")),

    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT})
)

