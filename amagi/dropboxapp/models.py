from django.db import models
from django.contrib.auth.models import User

class DropBoxUser(models.Model):
    user = models.ForeignKey(User)
    auth_code = models.CharField(max_length=255)
    dropbox_user_id = models.IntegerField(blank=True, null=True)
    dropbox_access_token = models.CharField(max_length=255, blank=True, null=True)
    added_date = models.DateTimeField('date added')

class UserFile(models.Model):
    user = models.ForeignKey(User)
    dropbox_user = models.ForeignKey(DropBoxUser)
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to='user_files')
    uploaded_date = models.DateTimeField('date uploaded')
