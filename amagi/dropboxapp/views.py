from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from dropboxapp.models import UserFile, DropBoxUser
from dropbox.client import DropboxOAuth2FlowNoRedirect, DropboxClient
from django.conf import settings
from django import forms
import datetime
from django.core.files.base import ContentFile
import thread

class RegisterForm(forms.Form):
    auth_code = forms.CharField(max_length=255)

class SyncForm(forms.Form):
    file_name = forms.CharField(max_length=255)

def index(request):
    current_user = request.user
    user_files = UserFile.objects.filter(user__id=current_user.id)
    folder_metadata = ''
    dropbox_files = None

    try:
        dropbox_user = DropBoxUser.objects.get(user__id=current_user.id)
        access_token = dropbox_user.dropbox_access_token
        client = DropboxClient(access_token)
        folder_metadata = client.metadata('/')
        dropbox_files = [content["path"] for content in folder_metadata["contents"]]
    except DropBoxUser.DoesNotExist:
        dropbox_user = ''

    template = loader.get_template('dropbox/index.html')
    form = SyncForm()

    context = RequestContext(request, {
        'files': user_files,
        'user': current_user,
        'dropbox_user': dropbox_user,
        'metadata': folder_metadata,
        'dropbox_files': dropbox_files,
        'form': form,
        'MEDIA_ROOT': settings.MEDIA_ROOT,
    })
    return HttpResponse(template.render(context))


def register(request):
    if request.method == 'POST':
        current_user = request.user
        form = RegisterForm(request.POST)
        if form.is_valid():
            try:
                DropBoxUser.objects.get(user=current_user)
            except DropBoxUser.DoesNotExist:
                auth_code = form.cleaned_data['auth_code']
                added_date = datetime.datetime.now()
                app_key = settings.APP_KEY
                app_secret = settings.APP_SECRET
                flow = DropboxOAuth2FlowNoRedirect(app_key, app_secret)
                flow.start()
                access_token, user_id = flow.finish(auth_code)
                DropBoxUser.objects.create(user=current_user,
                                           auth_code=auth_code,
                                           added_date=added_date,
                                           dropbox_user_id=user_id,
                                           dropbox_access_token=access_token,
                                           )
            template = loader.get_template('dropbox/index.html')
            context = RequestContext(request, {
                'user': current_user,
                'form': form,
                'MEDIA_ROOT': settings.MEDIA_ROOT,
            })
            return HttpResponse(template.render(context))
    else:
        current_user = request.user
        # Dropbox api flow
        app_key = settings.APP_KEY
        app_secret = settings.APP_SECRET
        flow = DropboxOAuth2FlowNoRedirect(app_key, app_secret)
        authorize_url = flow.start()
        template = loader.get_template('dropbox/register.html')
        form = RegisterForm()
        context = RequestContext(request, {
            'user': current_user,
            'authorize_url': authorize_url,
            'form': form,
            'flow': flow,
        })
    return HttpResponse(template.render(context))

def do_sync_bg(request):
    current_user = request.user
    form = SyncForm(request.POST)
    dropbox_user = None
    if form.is_valid():
        try:
            dropbox_user = DropBoxUser.objects.get(user=current_user)
        except DropBoxUser.DoesNotExist:
            pass # Handle if dropbox user was never created to begin with
        file_name = request.POST['file_name']
        uploaded_date = datetime.datetime.now()
        client = DropboxClient(dropbox_user.dropbox_access_token)
        f = client.get_file(file_name)
        cf = ContentFile(f.read())
        user_file = UserFile.objects.create(user=current_user,
                                dropbox_user=dropbox_user,
                                name=file_name,
                                uploaded_date=uploaded_date)
        user_file.file.save(file_name, cf)


def sync(request):
    if request.method == 'POST':
        thread.start_new_thread(do_sync_bg, (request,))
        current_user = request.user
        form = SyncForm(request.POST)
    else:
        current_user = request.user
        form = SyncForm()

    context = RequestContext(request, {
            'user': current_user,
            'form': form,
            'MEDIA_ROOT': settings.MEDIA_ROOT,
        })
    template = loader.get_template('dropbox/index.html')
    return HttpResponse(template.render(context))

