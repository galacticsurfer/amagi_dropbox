from django.contrib import admin
from dropboxapp.models import UserFile, DropBoxUser

admin.site.register(UserFile)
admin.site.register(DropBoxUser)