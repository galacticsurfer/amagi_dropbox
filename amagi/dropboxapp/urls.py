from django.conf.urls import patterns, url

from dropboxapp import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index')
)